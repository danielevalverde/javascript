//alert('funcionou');


var listElement = document.querySelector('#app ul');
var inputElement = document.querySelector('#app input');
var buttonElement = document.querySelector('#app button');

/*
1ª forma de fazer
var todos =[
    'Fazer café',
    'Estudar',
    'Assistir'
];

*/
var todos = JSON.parse(localStorage.getItem('list_todos') || []);
/*transforma de json para array
Como a lista pode começar vazia, sem ennhum TODO, irá dar erro por não ter nda a ser convertido
o [] serve pra informar que não há o que percorrer
*/



// IMPRIMINDO OS DADOS
function renderTodos(){
    listElement.innerHTML ='';
    // serve pra apagar tudo que estava contido na lista, caso contrário, toda vez que um novo todo fosse adicionado e renderizado, 
    for( todo of todos){
        var todoElement=document.createElement('li');
        var todoText = document.createTextNode(todo);

        var linkElement = document.createElement('a');
        // criei a tag
      
        linkElement.setAttribute('href','#');

        var pos = todos.indexOf(todo);
        linkElement.setAttribute('onclick', 'deleteTodo(' + pos + ')');
        var linkText=document.createTextNode('Excluir');
        // <a> Excluir</a>
   
        
        linkElement.appendChild(linkText);
        //
        todoElement.appendChild(todoText);
        todoElement.appendChild(linkElement);
        listElement.appendChild(todoElement);
        saveToStorage();
       
    }

}


renderTodos();

// ADICIONANDO OS DADOS
function addTodos(){
    var todoText= inputElement.value;
    todos.push(todoText);
    inputElement.value='';
    renderTodos();
    saveToStorage();
}

buttonElement.onclick = addTodos;




function deleteTodo(pos) {
    todos.splice(pos,1);
    // a função splice remove o elento do array a partir da posição pos
    // 0 1 define a quantidade de elementos a serem removidos
    renderTodos();
}

// ARMAZENANDO DADOS

function saveToStorage(){
     localStorage.setItem('list_todos', JSON.stringify(todos));
    /*

    A propriedade localStorage permite acessar um objeto Storage local. A localStorage é similar ao sessionStorage. A única diferença é que enquanto os dados armazenados no localStorage não expiram, os dados no sessionStorage tem os seus dados limpos ao expirar a sessão da página — ou seja, quando a página (aba ou janela) é fechada.
     stringify = transforma o vetor em string. Ppor que usamos isso? Porque o Storage não suporta um vetor
      o json.parse faz o inverso

     */

}



