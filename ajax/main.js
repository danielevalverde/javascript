// comente o segundo método para usar este
var xhr = new   XMLHttpRequest;
//cria uma objeto que será utilizado para a requisição ajax

xhr.open('GET', 'https://api.github.com/users/danielevalverde');
// define o método e a url e Inicializa um pedido. 


xhr.send(null);

xhr.onreadystatechange= function(){
    //função callback
    if(xhr.readyState===4){
        console.log(JSON.parse(xhr.responseText));
        // converte em objetivo JavaScript
    }
}


// segunda aula
/*
var minhaPromise = function(){
    return new Promise(function(resolve, reject) {
        //resolve quando o resultado for de sucesso e reject quando não
        xhr.open('GET', 'https://api.github.com/users/danielevalverde');

        xhr.send(null);

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
              if(xhr.status===200){
                  resolve(JSON.parse(xhr.responseText));
              }
              else{
                  reject('Erro na requisição');
              }
            }
        }

    });
}

var resultado = minhaPromise()
//console.log(resultado);
.then(function(response){
    console.log(response);
})
.catch(function(error){
    console.warn(error);
}); 


// ULTIMA AULA

axios.get('https://api.github.com/users/danielevalverde')
// encapsulamento da aula 02, 
    .then(function (response) {
        console.log(response);
    })
    .catch(function (error) {
        console.warn(error);
    });

*/

